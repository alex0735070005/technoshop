const fs = require('fs')
const FPG = require('fake-product-generator')
const rs = FPG(12) // creates a million products!
const ws = fs.createWriteStream('./products.json')
rs.pipe(ws)