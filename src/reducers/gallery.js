
let initialState = {
    images: [],
}

const gallery = (state = initialState, action) => {

    const {
        type,
        images
    } = action;

    switch (type) {
        case 'SET_IMAGES':
            return {
                ...state,
                images
            }
        default: return state
    }
}

export default gallery;