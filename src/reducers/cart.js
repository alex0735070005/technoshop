const initialState = {
    products: [],
    totalSum: 0,
    totalQuantity: 0,
    isShow: false
}

const cart = (state = initialState, action) => {

    switch (action.type) {
        case 'ADD_PRODUCT':
            return {
                ...state,
                products: [
                    ...state.products,
                    action.product
                ]
            }
        default: return state;
    }
}

export default cart;