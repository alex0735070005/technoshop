import { combineReducers } from 'redux';
import cart from './cart';
import gallery from './gallery';
import catalog from './catalog';


export default combineReducers (
    {
        cart,
        gallery,
        catalog
    }
)
