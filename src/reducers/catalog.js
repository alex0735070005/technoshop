const initialState = {
    products:[],
}

const catalog = (state = initialState, action) => {
    const {
        type,
        products,
    } = action;

    switch(type){
        case 'SET_PRODUCTS':
        return {
            ...state,
            products: [...products]
        }
        default: return state;
    }
}

export default catalog;