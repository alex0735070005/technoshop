export const addProductAction = (dispatch, product) => {
    dispatch({
        type: 'ADD_PRODUCT',
        product
    })
}