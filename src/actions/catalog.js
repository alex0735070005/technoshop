import data from '../data/products.json';

export const getProductsAction = (dispatch) => {
   
    new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(data);
        }, 5000)
    }).then(products => {
        dispatch({
            type: 'SET_PRODUCTS',
            products
        })
    })
}