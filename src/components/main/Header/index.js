import React from 'react';
import Menu from '../../modules/Menu';
import Cart from '../../modules/Cart';
import './style.scss';

const Header = () => {
    return (
        <header>
            <Menu />
            <Cart/>
        </header>
    );
}

export default Header;