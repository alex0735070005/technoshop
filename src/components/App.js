import React, {Component} from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from './main/Header';
import Footer from './main/Footer';
import Router from './Router';
import './App.scss';

class App extends Component {

  render() {    
    return (
      <BrowserRouter>
        <div className="App">
            <Header/>
            <Router/>
            <Footer/>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;