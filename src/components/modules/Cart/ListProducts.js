import React from 'react';
import Product from './Product';

const ListProducts = ({cartProducts}) => {
    return cartProducts.map((product)=>
        <Product 
            key={product.part_number}  
            data={product}
        />
    )
}

export default ListProducts;