import React from 'react';
import './style.scss';
import Header from './Header';
import Body from './Body';
import { connect } from 'react-redux';

const Cart = ({cartProducts}) => {
    return (
        <div id='cart'>
            <Header/>
            <Body
                cartProducts = {cartProducts}
            />
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        cartProducts: state.cart.products
    }
}

export default connect(
    mapStateToProps,
    null
)(Cart);