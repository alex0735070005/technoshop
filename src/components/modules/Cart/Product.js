import React from 'react';

const Product = (props) => {

    const {
        name,
        sum,
        count,
    } = props.data;

    return (
        <div className='product'>
            <div className='p-img'>
                <i className="fa fa-product-hunt" aria-hidden="true"></i>
                {' ' + name}
            </div>
            <div className='p-sum'>
                {sum} USD
            </div>
            <div className='p-quantity'>
                {count}
            </div>
            <div className='btn-remove'>
                <i className="fa fa-trash-o" aria-hidden="true"></i>
            </div>
        </div>
    );
}

export default Product;