import React from 'react';

const Header = () => {
    return (
        <div className='cart-header'>
            <i className="fa fa-cart-arrow-down" aria-hidden="true"></i>
            <span className="total-sum">Sum: 3000 USD</span>
            <span className="total-count">Count: 5</span>
        </div>
    );
}

export default Header;