import React from 'react';
import ListProducts from './ListProducts';

const Body = ({cartProducts}) => {
   
    return (
       <div className='cart-body'>
          <ListProducts
            cartProducts = {cartProducts}
          />
       </div>
    );
}

export default Body;