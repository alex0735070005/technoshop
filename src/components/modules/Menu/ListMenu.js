import React from 'react';
import menu from './menu.json';
import { NavLink } from 'react-router-dom';

const ListMenu = () => menu.map(el => 
    <li key={el.link} >
        <NavLink 
            to={el.link}
            exact
            activeClassName="selected"
        >
            {el.label}
        </NavLink>
    </li>
);

export default ListMenu;