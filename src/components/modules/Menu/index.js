import React from 'react';
import ListMenu from './ListMenu';
import './style.scss';

const Menu = () => {
    return (
        <ul id="menu-nav">
            <ListMenu />
        </ul>
    );
}

export default Menu;