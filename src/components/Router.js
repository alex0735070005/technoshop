import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import Catalog from './pages/Catalog';
import About from './pages/About';
import Gallery from './pages/Gallery';
import Contacts from './pages/Contacts';
import Calculator from './pages/Calculator';

export default () => (
    <Switch>
        <Route exact path='/'       component={Home}/>
        <Route path='/catalog'      component={Catalog} />
        <Route path='/about-us'     component={About} />
        <Route path='/gallery'      component={Gallery} />
        <Route path='/contacts'     component={Contacts} />
        <Route path='/calculator'   component={Calculator} />
    </Switch>
);