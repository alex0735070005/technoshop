import React from 'react';
import Product from './Product';

const ListProducts = ({products, addProduct}) => {
    
    return products.map((product) => {
       return <Product
            key={product.part_number} 
            product={product} 
            addProduct={addProduct}
        />
    })
}

export default ListProducts;