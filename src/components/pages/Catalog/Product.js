import React from 'react';

const Product = ({ product, addProduct }) => {

    const {
        name,
        image,
        price,
        vendor_description,
    } = product;

    const add = ()=> {
        addProduct(product);
    }

    return (
        <div className='product'>
            <div className='w-prod'>
                <h3>{name}</h3>
                <img src={image} alt='' />
                <p>
                    {vendor_description.slice(0, 150) + ' '}
                    <a href='#' >more{' ...'}</a>
                </p>
                <div className='price'>
                    <i>{price}</i>
                    <button
                        type='button'
                        onClick={add}
                    >
                        Add cart
                    </button>
                </div>
            </div>
        </div>
    );
}

export default Product;