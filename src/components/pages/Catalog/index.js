import React, { Component } from 'react';
import ListProducts from './ListProducts';
import { connect } from 'react-redux';
import { addProductAction } from '../../../actions/cart';
import { getProductsAction } from '../../../actions/catalog';
import './style.scss';

class Catalog extends Component {

    constructor(props) {
        super(props);
        console.log('NEW CATALOG');
    }

    componentDidMount() {
        this.props.getProducts();
    }

    render() {
        const {
            addProduct,
            products
        } = this.props;

        return (
            <div id="catalog" className="page">
                <div className='products-list'>
                    <ListProducts
                        products={products}
                        addProduct={addProduct}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ catalog: { products } }) => {
    return {
        products
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProduct: data => {
            addProductAction(dispatch, data);
        },
        getProducts: () => {
            getProductsAction(dispatch);
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Catalog);