import React from 'react';
import { connect } from 'react-redux';
import { getImagesAction } from '../../../actions/gallery';
import './style.scss';

const Gallery = (props) => {
    props.getImages();
    return (
        <div id="gallery" className="page">
            <h1>Gallery</h1>
        </div>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        getImages:()=> {
            getImagesAction(dispatch)
        }
    }
 }

export default connect(null, mapDispatchToProps)(Gallery);