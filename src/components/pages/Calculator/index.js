import React, { Component } from 'react';
import './style.scss';

class Calculator extends Component {

    constructor(props) {
        super(props);

        this.state = {
            firstValue: 0,
            secondValue: 0,
            operationValue: '+',
            sumValue: 0
        }
    }

    fistChange = (event) => {
        const {
            operationValue,
            secondValue,
        } = this.state;

        let firstValue = event.target.value;

        this.setState({
            firstValue,
        });

        this.countResult({
            firstValue,
            operationValue,
            secondValue,
        });
    }

    operationChange = (event) => {
        const {
            firstValue,
            secondValue
        } = this.state;

        let operationValue = event.target.value;

        this.setState({
            operationValue,
        });
        this.countResult({
            firstValue,
            operationValue,
            secondValue,
        });
    }

    secondChange = (event) => {

        const {
            firstValue,
            operationValue,
        } = this.state;

        let secondValue = event.target.value;

        this.setState({
            secondValue,
        });
        this.countResult({
            firstValue,
            operationValue,
            secondValue,
        });
    }

    countResult(data) {
        const {
            firstValue,
            secondValue,
            operationValue,
        } = data;

        if (firstValue && secondValue && operationValue) {
            this.setState({
                sumValue: this.sum(
                    firstValue,
                    operationValue,
                    secondValue
                )
            });
        }
    }

    sum(a, o, c) {
        switch (o) {
            case '-': return parseInt(a) - parseInt(c);
            case '/': return parseInt(a) / parseInt(c);
            case '*': return parseInt(a) * parseInt(c);
            default: return parseInt(a) + parseInt(c);
        }
    }

    reset = () => {
        this.setState({
            firstValue: 0,
            operationValue: '+',
            secondValue: 0,
            sumValue: 0,
        });
    }

    render() {
        return (
            <div id="calculator" className="page">
                <div className='c-form'>
                    <h3>Calculator</h3>
                    <input
                        placeholder='First number'
                        onChange={this.fistChange}
                        value={this.state.firstValue}
                    />
                    <input
                        placeholder='Operatin +-/*'
                        onChange={this.operationChange}
                        value={this.state.operationValue}
                    />
                    <input
                        placeholder='Second number'
                        onChange={this.secondChange}
                        value={this.state.secondValue}
                    />
                    <input
                        readOnly
                        value={this.state.sumValue}
                    />
                    <input
                        type='button'
                        onClick={this.reset}
                        value='Recet'
                    />
                </div>
                first: {this.state.firstValue} <br />
                operation: {this.state.operationValue}<br />
                second: {this.state.secondValue}<br />
            </div>
        );
    }
}

export default Calculator;